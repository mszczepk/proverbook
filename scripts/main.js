window.addEventListener("load", init, false);
var proverbParagraph;

function init() {
    setupProverbParagraph();
    loadScript("scripts/database.js", setRandomProverb);
}

function setupProverbParagraph() {
    proverbParagraph = document.getElementById("proverb-paragraph");
    proverbParagraph.addEventListener("click", setRandomProverb, false);
}

function loadScript(src, callback) {
    var script = document.createElement("script");
    script.type = "text/javascript";
    script.src = src;
    script.addEventListener("load", callback, false);
    script.addEventListener("statechange", callback, false);

    document.head.appendChild(script); //fire script execution
}

function setRandomProverb() {
    var length = database.length;
    var index1 = Math.floor((Math.random() * length));
    var index2 = Math.floor((Math.random() * length));
    proverbParagraph.innerHTML = database[index1][0] + " " + database[index2][1] + ".";
}
